import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { userApi } from "../../api/userApi";

const initialState = {
  isLoading: false,
  currentUser: null,
};

export const userSlice = createSlice({
  name: "user_slice ",
  initialState,
  reducers: {
    resetActiveCategory: (state) => {
      state.value = 0;
    },
    defaultActiveTab: (state, action) => {
      state.value = action.payload;
    },
  },
  extraReducers: (builder) => {
    // Bắt đầu thực hiện action (Promise pending)
    builder.addCase(fetchUserById.pending, (state) => {
      state.isLoading = true;
    });

    // Khi thực hiện action thành công (Promise fulfilled)
    builder.addCase(fetchUserById.fulfilled, (state, action) => {
      state.isLoading = false;
      state.currentUser = action.payload;
    });

    // Khi thực hiện action thất bại (Promise rejected)
    builder.addCase(fetchUserById.rejected, (state) => {
      state.isLoading = false;
    });
  },
});

// Action creators are generated for each case reducer function
export const { resetActiveCategory, defaultActiveTab } = userSlice.actions;

export default userSlice.reducer;

export const fetchUserById = createAsyncThunk(
  "user/fetchUserById",
  async (userId) => {
    const response = await userApi.getUser(userId);
    return response.data;
  }
);
